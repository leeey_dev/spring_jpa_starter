package app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.domain.Employee;
import app.service.EmployeeService;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	/**
	 * get employee info by emloyeeNo
	 *  
	 * @param employeeNo
	 * @return
	 */
	@GetMapping("/{employeeNo}")
	public Optional<Employee> get(@PathVariable Integer employeeNo) {
		return employeeService.getEmployee(employeeNo);
	}
	
	/**
	 * add employee info by Employee class
	 * 
	 * @param employee
	 * @return
	 */
	@PostMapping
	public Employee add(@RequestBody Employee employee) {
		
		return employeeService.save(employee);
		
	}
	
	/**
	 * add employee Info by employee name
	 * 
	 * @param employeeName
	 * @return
	 */
	@PostMapping("/{employeeName}")
	public Employee add2(@PathVariable String employeeName) {
		
		return employeeService.save(new Employee(employeeName));
		
	}
	
	/**
	 * delete employee info by employeeNo
	 * 
	 * @param employeeNo
	 */
	@DeleteMapping("/{employeeNo}")
	public void delete(@PathVariable Integer employeeNo) {
		employeeService.delete(employeeNo);
	}
	
	/**
	 * get All employees
	 * 
	 * @return
	 */
	@GetMapping
	public List<Employee> getAll() {
		return employeeService.getAllEmployees();
	}
	

}
