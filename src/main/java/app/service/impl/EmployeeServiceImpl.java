package app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.domain.Employee;
import app.repository.EmployeeRepository;
import app.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employee save(Employee employee) {
		
		return employeeRepository.save(employee);
	}

	@Override
	public void delete(Integer employeeNo) {
		
		employeeRepository.deleteById(employeeNo);
		
	}

	@Override
	public Optional<Employee> getEmployee(Integer employeeNo) {
		
		return employeeRepository.findById(employeeNo);
	}

	@Override
	public List<Employee> getAllEmployees() {
		
		return employeeRepository.findAll();
	}

}
