package app.service;

import java.util.List;
import java.util.Optional;

import app.domain.Employee;

public interface EmployeeService {
	
	Employee save(Employee employee);
	
	void delete(Integer employeeNo);
	
	Optional<Employee> getEmployee(Integer employeeNo);
	
	List<Employee> getAllEmployees();
}
