package app.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
public @Data class Employee implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -5864462395929969495L;
	
	
	@Id
	@GeneratedValue
	private Integer no;
	private String name;

	public Employee() {

	}

	public Employee(String name) {
		this.name = name;
	}

}
