package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.domain.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
